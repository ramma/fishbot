#!/usr/bin/python
"""!version - Fishbot's version and other info."""
import fishapi

def bang(pipein, arguments, event):
    return (fishapi.version + " - git repo at https://gitlab.com/chizu/fishbot", None)
